#!/bin/bash
echo "[CGVault] Begin installation"
echo "[CGVault] What is the empty git repository where you wanna store your credentials ? (required)"
read REPOSITORY
if [ -z $REPOSITORY ]; then
    echo "[CGVault] Git repository is required !"
    echo "[CGVault] Exiting ..."
    exit 1
fi

echo "[CGVault] What is the CLI command name do you want for call cgvault ? (default: cgvault)"
read CLI_NAME
if [ -z $CLI_NAME ]; then
    CLI_NAME="cgvault"
fi

echo "[CGVault] What is the path do you want for install cgvault ? (default: ${HOME}/.cgvault)"
read INSTALL_PATH
if [ -z $INSTALL_PATH ]; then
    INSTALL_PATH="${HOME}/.cgvault"
fi

echo "[CGVault] We going to install dependencies ..."
echo "[CGVault] Execute:  brew install python"
brew install python -y
echo "[CGVault] Execute: sudo pip install awscli ansible"
sudo pip install awscli ansible -y

echo "[CGVault] Dependencies installed"

echo "[CGVault] Clone repository ..."
echo "[CGVault] Execute: git clone https://gitlab.com/bliiitz/cgvault.git ${INSTALL_PATH}"
git clone https://gitlab.com/bliiitz/cgvault.git $INSTALL_PATH
cd $INSTALL_PATH

echo "[CGVault] Change git origin ..."
cd $INSTALL_PATH
git remote rm origin
git remote add origin $REPOSITORY

echo "[CGVault] Push to new origin (you probably need to set your git credentials)"
git push origin master

echo "[CGVault] Start Ansible init script"
ansible-playbook main.yml --tags=init \
    -e "cgvault_path=${INSTALL_PATH}" \
    -e "cgvault_cli_name=${CLI_NAME}"


echo "[CGVault] Use '${CLI_NAME}' for use CGVault !"
echo "[CGVault] Installation script ended ! Enjoy !"
